﻿#include <iostream>
#include <string>
using namespace std;

class Player
{
private:

    string name;
    int score = 0;

public:

    void Enter()
    {
        cout << "Player name" << "\n";
        cin >> name;

        cout << "Player score" << "\n";
        cin >> score;
    }

    static void sort(Player* array, Player* x, int a)
    {
        for (int j = 0; j < (a - 1); j++)
        {
            for ( Player* i = array; i < (x - 1); i++)
                if (i->score < (i + 1)->score)
                {
                    swap(*i, *(i + 1));
                }
        }
    }

    void Out()
    {
        cout << name << " " << score <<"\n";
    }
};

int main()
{
    int a = 0;

    cout << "How many players do you want to add?" << "\n";
    cin >> a;

    Player* Players = new Player[a];

    for (int i = 0; i < a; i++)
    {
        Players[i].Enter();
    }

    Player::sort(Players, Players + a, a);

    for (int i = 0; i < a; i++)
    {
        Players[i].Out();
    }
    
    delete[] Players;
    Players = nullptr;

    return 0;
}